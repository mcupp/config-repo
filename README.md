# Test Config Repo

This is a test repository for use with the [config-server](https://code.vt.edu/mcupp/config-server) POC project.
Property files have a naming format of `{clientAppName}-some-env.properties`. For instance the
`gateway-dev-cloud-config.properties` is resolved by the config server by extracting the `name` from the first word
and the `profile` from the remaining file name.
